package com.szkolenie.enums;

public enum Answer {
    POSITIVE("tak", "yes", "y", "t", "ok"),
    NEGATIVE("nie", "no", "n", "bad", "wrong");

    private final String[] synonyms;

    Answer(String... synonyms) {
        this.synonyms = synonyms;
    }

    public boolean isSynonym(String word) {
        word = word.trim().toLowerCase();
        for (String synonym : this.synonyms) {
            if (synonym.equals(word)) {
                return true;
            }
        }
        return false;
    }
}
